//
//  DetailViewController.swift
//  MyVideoPlayer
//
//  Created by Abdul Rehman on 12/4/19.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit
import AVKit

class DetailViewController: BaseController , UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    var video : Video = Video.init()
    
    var materialArray : NSArray = []
    

    @IBOutlet weak var minimizedVideoParent: UIView!
    @IBOutlet weak var fullscreenmainparent: UIView!
    @IBOutlet weak var fullScreenPlayerParentView: UIView!
    @IBOutlet weak var otherVideosCollectionView: UICollectionView!
    
    @IBOutlet weak var initialthumbImage: UIImageView!
    
    @IBOutlet weak var initialDescription: UILabel!
    
    var fullScreenPlayerLayer = AVPlayerLayer()
    
    
    var player : AVPlayer = AVPlayer.init()
    var fullscreenPlayer : AVPlayer = AVPlayer.init()
    
    @IBOutlet weak var videoPlayerParent: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fullscreenmainparent.alpha = 0
        minimizedVideoParent.alpha = 0
        initialthumbImage.image = UIImage.init(named: video.thumbNail as String)
        initialDescription.text = video.videoDescription as String
        otherVideosCollectionView.reloadData()
        addLeftGestureToMinizedView()
    }
    
    
    /*
     adding left swipe gesture on minized view
     */
    
    func addLeftGestureToMinizedView() -> Void {
        let swipe: UISwipeGestureRecognizer = UISwipeGestureRecognizer.init(target: self, action:#selector(handleGesture))
        swipe.direction = .left
        minimizedVideoParent.addGestureRecognizer(swipe)
    }
    
    
    /*
     Handling swipe gesture
     */
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
       if gesture.direction == .left {
            print("Swipe Left")
        fullscreenPlayer.pause()
        minimizedVideoParent.alpha = 0
        
        }
       
    }
    
    
    /*
     Handling IBActions
     */
    
    @IBAction func pausetapped(_ sender: Any) {
        player.pause()
    }
    @IBAction func resizeTapped(_ sender: Any) {
        
        fullscreenmainparent.alpha = 1
        player.pause()
        
        guard let path = Bundle.main.path(forResource: video.videoUrl as String, ofType:"mp4") else {
            debugPrint("video.m4v not found")
            return
        }
        
        fullscreenPlayer = AVPlayer(url: URL(fileURLWithPath: path))
        fullScreenPlayerLayer = AVPlayerLayer(player: fullscreenPlayer)
        fullScreenPlayerLayer.frame = fullScreenPlayerParentView.bounds
        fullScreenPlayerParentView.layer.addSublayer(fullScreenPlayerLayer)
        
        fullscreenPlayer.play()
        
    }
    
    @IBAction func resumeTapped(_ sender: Any) {
        player.play()
    }
    @IBAction func playInitialVideo(_ sender: Any) {
        
        
        guard let path = Bundle.main.path(forResource: video.videoUrl as String, ofType:"mp4") else {
            debugPrint("video.m4v not found")
            return
        }
        player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = videoPlayerParent.frame
        videoPlayerParent.layer.addSublayer(playerLayer)
        player.play()
        
    }
    
    
    @IBAction func mainPauseTapped(_ sender: Any) {
        fullscreenPlayer.pause()
    }
    
    @IBAction func mainPlayTapped(_ sender: Any) {
        fullscreenPlayer.play()
        
    }
    
    @IBAction func mainSeektapped(_ sender: Any) {
        
        
        let timeScale = fullscreenPlayer.currentItem?.asset.duration.timescale;
        //        let seconds = kCMTimeZero
        let time = CMTimeMakeWithSeconds( 4.0 , preferredTimescale: timeScale!)
        fullscreenPlayer.seek(to: time, toleranceBefore: time, toleranceAfter: CMTime.zero)
    }
    
    
    
    
    @IBAction func dismissFullSize(_ sender: Any) {
       animateMinimizeVideo()
    }
    
    
    func animateMinimizeVideo() -> Void {
        UIView.animate(withDuration: 0.7) {
            self.fullscreenPlayer.pause()
            self.fullscreenmainparent.alpha = 0
            self.minimizedVideoParent.alpha = 1

            guard let path = Bundle.main.path(forResource: self.video.videoUrl as String, ofType:"mp4") else {
                debugPrint("video.m4v not found")
                return
            }
            self.fullscreenPlayer.pause()
            self.fullScreenPlayerLayer.removeFromSuperlayer()
            self.fullscreenPlayer = AVPlayer(url: URL(fileURLWithPath: path))
            self.fullScreenPlayerLayer = AVPlayerLayer(player: self.fullscreenPlayer)
            self.fullScreenPlayerLayer.frame = self.minimizedVideoParent.bounds
            self.minimizedVideoParent.layer.addSublayer(self.fullScreenPlayerLayer)
            self.fullscreenPlayer.play()
            
        }
    }
    
    @IBAction func minimizedTapped(_ sender: Any) {
        print("asd")
        minimizedVideoParent.alpha = 0
        resizeTapped("")
    }
    
    /*
     Collection view delegate methods
     */
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return materialArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : OtherVideosCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "OtherVideosCollectionViewCell", for: indexPath) as! OtherVideosCollectionViewCell
        
        let video : Video = materialArray[indexPath.row] as! Video
        cell.videoImageView.image = UIImage.init(named: "\(video.thumbNail)")
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 150,height: 130);
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        video  = materialArray[indexPath.row] as! Video
        guard let path = Bundle.main.path(forResource: video.videoUrl as String, ofType:"mp4") else {
            debugPrint("video.m4v not found")
            return
        }
        fullscreenPlayer.pause()
        fullScreenPlayerLayer.removeFromSuperlayer()
        fullscreenPlayer = AVPlayer(url: URL(fileURLWithPath: path))
        fullScreenPlayerLayer = AVPlayerLayer(player: fullscreenPlayer)
        fullScreenPlayerLayer.frame = fullScreenPlayerParentView.bounds
        fullScreenPlayerParentView.layer.addSublayer(fullScreenPlayerLayer)
        
        fullscreenPlayer.play()
    }
    
}
