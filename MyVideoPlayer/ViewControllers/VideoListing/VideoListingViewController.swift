//
//  VideoListingViewController.swift
//  MyVideoPlayer
//
//  Created by Abdul Rehman on 11/26/19.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit

class VideoListingViewController: BaseController , UITableViewDelegate, UITableViewDataSource {
    
    /*
        Outlets for class
     */
    
    @IBOutlet weak var mainTableView: UITableView!
    var thumbNails : NSMutableArray = []
    var videos : NSArray = []
    

    override func viewDidLoad() {
        super.viewDidLoad()

        thumbNails = NSMutableArray.init()
        var videos: [Video] = []
        self.navigationItem.title = "My Videos"
        ShowLoading(view: view)
        mainTableView.reloadData()
        HideLoading(view: view)
        
        addMaterial()
        mainTableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    
    /*
     Adding material data for videos from Data Manager
     */
    func addMaterial() -> Void {
        videos = DataManager.loadVideos()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MainVideoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "videoCell") as! MainVideoTableViewCell
        
        let video : Video = videos[indexPath.row] as! Video
        
        
        cell.thumbNailImage.image = UIImage.init(named: "\(video.thumbNail)")
        cell.videoNameLabel.text = video.videoName as String
        cell.videoDescriptionLabel.text = video.videoDescription as String
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 156
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller :  DetailViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        controller.materialArray = videos
        controller.video = videos[indexPath.row] as! Video
        self.navigationController?.pushViewController(controller, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
