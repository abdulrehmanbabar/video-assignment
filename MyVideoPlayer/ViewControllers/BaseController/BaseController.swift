//
//  BaseController.swift
//  MyVideoPlayer
//
//  Created by Abdul Rehman on 11/26/19.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit
import LoadingShimmer


class BaseController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    func ShowLoading(view : UIView) -> Void {
        LoadingShimmer.startCovering(view)
    }
    func HideLoading(view : UIView) -> Void {
        LoadingShimmer.stopCovering(view)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
