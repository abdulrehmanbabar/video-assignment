//
//  MainVideoTableViewCell.swift
//  MyVideoPlayer
//
//  Created by Abdul Rehman on 12/4/19.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit

class MainVideoTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var thumbNailImage: UIImageView!
    
    @IBOutlet weak var videoNameLabel: UILabel!
    
    @IBOutlet weak var videoDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
