//
//  OtherVideosCollectionViewCell.swift
//  MyVideoPlayer
//
//  Created by Abdul Rehman on 12/4/19.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit

class OtherVideosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var videoImageView: UIImageView!
}
