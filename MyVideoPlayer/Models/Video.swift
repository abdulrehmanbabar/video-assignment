//
//  Video.swift
//  MyVideoPlayer
//
//  Created by Abdul Rehman on 12/6/19.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit

class Video: NSObject {
    
    var videoName : NSString
    var videoUrl  : NSString
    var thumbNail  : NSString
    var videoDescription :  NSString
    
    
    override init() {
        videoName = ""
        videoUrl = ""
        thumbNail = ""
        videoDescription = ""
    }
   
    
    init(url: NSString, thumbURL: NSString, name: NSString, description: NSString) {
        self.videoName = name
        self.videoUrl = url
        self.videoDescription = description
        self.thumbNail = thumbURL
        super.init()
    }
    
    /*
     making new viedo objects and returning
     */
    
    class func newVideo(url: NSString, thumbURL: NSString, name: NSString, description: NSString) -> Video {
        var video : Video = Video(url: url,thumbURL: thumbURL,name: name,description: description)
        return video
    }

    
}
